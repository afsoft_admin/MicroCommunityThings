package com.java110.barrier.engine;

import com.java110.entity.car.CarInoutTempAuthDto;
import com.java110.entity.machine.MachineDto;
import com.java110.entity.parkingArea.ParkingAreaDto;
import com.java110.entity.parkingArea.ResultParkingAreaTextDto;
import com.java110.intf.inner.IInOutCarTextEngine;

import java.util.List;

public interface IInCarEngine {

    ResultParkingAreaTextDto enterParkingArea(String type, String carNum, MachineDto machineDto, List<ParkingAreaDto> parkingAreaDtos, IInOutCarTextEngine inOutCarTextEngine) throws Exception;

    void tempCarAuthOpen(CarInoutTempAuthDto carInoutTempAuthDto) throws Exception;
}
